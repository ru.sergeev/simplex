//
// Created by Dmitry Sergeev on 10/20/16.
//

#ifndef SIMPLEX_FLOW_EDGE_H
#define SIMPLEX_FLOW_EDGE_H

#include <stdexcept>

template<typename value_t>
struct flow_edge
{
	static_assert(std::is_scalar<value_t>::value, "template argument must be a scalar type");
public:
    flow_edge(int from, int to, value_t capacity) : _from(from), _to(to), _capacity(capacity), _flow(0)
    { }

	int from() const { return _from; }
	int to() const { return _to; }
	value_t capacity() const { return _capacity; }
	value_t flow() const { return _flow; }

	int other(int vertex) const {
        if (vertex == _from) return _to;
        else if (vertex == _to) return _from;
        else throw std::invalid_argument("vertex");
    }

	value_t residual_capacity_to(int vertex) const{
        if (vertex == _from) return _flow;
        else if (vertex == _to) return _capacity - _flow;
		else throw std::invalid_argument("vertex");
    }
    void add_residual_flow_to(int vertex, value_t delta) {
        if (vertex == _from) _flow -= delta;
        else if (vertex == _to) _flow += delta;
		else throw std::invalid_argument("vertex");
    }
private:
    int _from;
    int _to; // from and to
    value_t _capacity; // capacity
    value_t _flow; // flow
};

#endif //SIMPLEX_FLOW_EDGE_H
