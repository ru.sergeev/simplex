//
// Created by Dmitry Sergeev on 10/24/16.
//

#ifndef SIMPLEX_FISH_FRYER_H
#define SIMPLEX_FISH_FRYER_H

#include <algorithm>
#include <numeric>
#include <string>
#include <functional>

#include <map>
#include <set>

#include "ford_fulkerson.h"
#include "order.h"

std::string timestampToString(int timestep){
    return std::to_string(timestep);
}

struct fryers{

    fryers(int now){
        for(auto & t : t_ready){ t = now;}
    }
    int ready(const int index, int now) const{
        return std::max(now, t_ready[index]);
    }
    int capacity(const int index, int now, int allowed = 600){
        return allowed + now - ready(index, now);
    }
    int t_ready[5]; // 0..3 - fish, 4 - chips


    bool can_do(struct order &order, int within){

        flow_network<int> fn = make_network(order, within);

        int can_produce_cod, can_produce_haddock, can_produce_chips;
        for(auto &e:fn.adj(sink)){
            switch(e->from()){
                case 33://cod_check:
                    can_produce_cod = e->flow();
                    break;
                case 34://haddock_check:
                    can_produce_haddock = e->flow();
                    break;
                default:
                    can_produce_chips = e->flow();
                    break;
            }
        }

        return can_produce_cod == order.cod*80 &&  can_produce_haddock == order.haddock*90 && can_produce_chips == order.chips/4*120;

    }

    void cook(struct order & order){
        auto sec = ready_in(order);
        if (sec > 600){
            std::cout << "at " << timestampToString(order.time) << ", Order #" << order.id  << " Rejected" << std::endl;
        }else{
            std::cout << "at " << timestampToString(order.time) << ", Order #" << order.id  << " Accepted" << std::endl;
            auto schedule = report(order, sec);
            for(auto t : schedule.timeline){
                std::cout << "at " << timestampToString(order.time) << ", " << schedule.at(t) << std::endl;

            }
            for(int i = 0; i < 5; i++)
            { t_ready[i] += schedule.delta[i];}
        }
        //std::cout << "ready in " <<  sec << " sec" << std::endl;
    }

    struct schedule{
        std::map<int, int> cod;
        std::map<int, int> haddock;
        std::map<int, int> chips;
        std::set<int, std::greater<int> > timeline;
        int delta[5] = {0,0,0,0,0};
        void insert(int time, int pan, int what, int count = 1){
            switch (what){
                case 1:
                    if (cod.find(time) == cod.end())
                    { cod[time] = 0;}
                    cod[time]+= count;
                    timeline.insert(time);
                    delta[pan] += 80;
                    break;
                case 2:
                    if (haddock.find(time) == haddock.end())
                    { haddock[time]=0; }
                    haddock[time]+= count;
                    timeline.insert(time);
                    delta[pan] += 90;
                    break;
                case 3:
                    if (chips.find(time) == chips.end())
                    { chips[time]=0; }
                    chips[time]+= count;
                    timeline.insert(time);
					delta[pan] += 120;
                    break;
            }
        }
        std::string at(int index){
            std::string result = "Begin Cooking ";
            if (cod.find(index) != cod.end()) result = std::to_string(cod[index]) + " Cod";
            if (haddock.find(index) != haddock.end()) result = std::to_string(haddock[index]) + " Haddock";
            if (chips.find(index) != chips.end()) result = std::to_string(chips[index]) + " Chips";
            return result;
        }
    };

    struct schedule report(struct order &order, int within){
        flow_network<int> fn = make_network(order, within);
        struct schedule sch;

        for(auto i = 0; i < 4; i++) {
            int offset = 0;
            for(auto &e: fn.adj(fresh[i])){
                if(e->to() == cod_fresh[i] && e->flow() == 0) {
                    offset = 80;
                    auto time = order.time+within-offset;
                    sch.insert(time, i, 1);
                }
                if(e->to() == haddock_fresh[i] && e->flow() == 0) {
                    offset = 90;
                    auto time = order.time + within - offset;
                    sch.insert(time, i, 2);
                }
            }
            for(auto &e: fn.adj(warm[i])){
                if(e->to() == cod_warm[i] && e->flow() == 0){
                    auto time = order.time+within-80-offset;
                    sch.insert(time, i, 1);
                }
                if(e->to() == haddock_warm[i] && e->flow() == 0){
                    auto time = order.time + within - 90 - offset;
                    sch.insert(time, i, 2);
                }
            }

        }
        auto chips = order.chips;
        auto offset = 0;
        while (chips > 0){
            offset +=120;
            auto time = order.time + within - offset;
            sch.insert(time, 5, 3, (chips>4) ? 4 : chips);
            chips -=4;
        }

        return sch;
    }

    int ready_in(struct order& order){
        int time = 600;
        auto min_time = 0;
        auto max_time = 600;
        int middle = (min_time - max_time)/2;

        return binary_search( order, 0, 601);
    }

private:

    int binary_search(struct order& order, int begin, int end){
        auto mid = (begin+end)/2;
        if(mid==end) return mid;
        if (can_do(order, mid)){
            return binary_search(order, begin, mid);
        }else return binary_search(order, mid+1, end);
    }

    const int for_chips = 4;
    const int source = 0;
    const int cod = 1;
    const int haddock = 2;
    const int chips = 3;
    const int merge[4] = {35,36,37,38};
    const int pan[5] = {4,5,6,7,8};
    const int fresh[4] = {9,10,11,12};
    const int warm[4] = {13,14,15,16};
    const int cod_fresh[4] = {17,18,19,20};
    const int cod_warm[4] = {21,22,23,24};
    const int haddock_fresh[4] = {25,26,27,28};
    const int haddock_warm[4] = {29,30,31,32};
    const int cod_check = 33;
    const int haddock_check = 34;
    const int sink = 39;
    const int products[3] = {cod, haddock, chips};


    flow_network<int> make_network(struct order &order, int within = 600){


        flow_network<int> fn(sink+1);
        fn.add_edge(std::move(flow_edge<int>{source, cod, order.cod*80}));
        fn.add_edge(std::move(flow_edge<int>{source, haddock, order.haddock*90}));
        fn.add_edge(std::move(flow_edge<int>{source, chips, order.chips/4*120}));
        for(auto i = 0; i < 4; i++){
            fn.add_edge(std::move(flow_edge<int>{cod, merge[i], 2*80}));
            fn.add_edge(std::move(flow_edge<int>{haddock, merge[i], 2*90}));

            fn.add_edge(std::move(flow_edge<int>{merge[i], pan[i], capacity(i, order.time, within)}));

            fn.add_edge(std::move(flow_edge<int>{pan[i], fresh[i], 120})); // 2 (no more than 120 sec)
            fn.add_edge(std::move(flow_edge<int>{fresh[i], cod_fresh[i], 80}));
            fn.add_edge(std::move(flow_edge<int>{fresh[i], haddock_fresh[i], 90}));

            fn.add_edge(std::move(flow_edge<int>{pan[i], warm[i], 120})); // 2 (no more than 120 sec)
            fn.add_edge(std::move(flow_edge<int>{warm[i], cod_warm[i], 80}));
            fn.add_edge(std::move(flow_edge<int>{warm[i], haddock_warm[i], 90}));

            // prefer fresh
            fn.add_edge(std::move(flow_edge<int>{cod_warm[i], cod_fresh[i], 80}));
            fn.add_edge(std::move(flow_edge<int>{haddock_warm[i], haddock_fresh[i], 90}));


            fn.add_edge(std::move(flow_edge<int>{cod_fresh[i], cod_check, 80}));
            fn.add_edge(std::move(flow_edge<int>{cod_warm[i], cod_check, 80}));

            fn.add_edge(std::move(flow_edge<int>{haddock_fresh[i], haddock_check, 90}));
            fn.add_edge(std::move(flow_edge<int>{haddock_warm[i], haddock_check, 90}));

        }
        fn.add_edge(std::move(flow_edge<int>{cod_check, sink, order.cod*80}));
        fn.add_edge(std::move(flow_edge<int>{haddock_check, sink, order.haddock*90}));


        fn.add_edge(std::move(flow_edge<int>{chips, pan[for_chips], capacity(for_chips, order.time, within)}));
        fn.add_edge(std::move(flow_edge<int>{pan[for_chips], sink, 240})); // 8 -> 2x4 ( no more than 120 sec )

        ford_fulkerson<int>(fn, source, sink);

        return fn;
    }

};

#endif //SIMPLEX_FISH_FRYER_H
