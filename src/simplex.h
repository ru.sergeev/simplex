//
// Created by Dmitry Sergeev on 10/18/16.
//

#ifndef SIMPLEX_SIMPLEX_H
#define SIMPLEX_SIMPLEX_H

#include <vector>

class simplex {
public:
    simplex(const std::vector<std::vector<double>> &A, const std::vector<double> &b, const std::vector<double> &c) : m(b.size()), n(c.size()){
        for (int i = 0; i < m; i++) {
            std::vector<double> row(n+m+1, 0.0);
            a.emplace_back(row);
            for (int j = 0; j < n; j++)
                a[i][j] = A[i][j];
        }
        std::vector<double> row(n+m+1, 0.0);
        a.emplace_back(row);
        for (int j = n; j < m + n; j++) a[j-n][j] = 1;
        for (int j = 0; j < n; j++) a[m][j] = c[j];
        for (int i = 0; i < m; i++) a[i][m+n] = b[i];
    }
    const std::vector<std::vector<double>>& solve() {
        while (true) {
            int q = bland();
            if (q == -1) break;
            int p = minRatioRule(q);
            if (p == -1) throw "Hm";
            pivot(p, q);
        }
        return a;
    }

private:
    int bland() {
        for (int q = 0; q < m + n; q++)
            if (a[m][q] > 0) return q;
        return -1;
    }
    int minRatioRule(int q) {
        int p = -1;
        for (int i = 0; i < m; i++) {
            if (a[i][q] <= 0) continue;
            else if (p == -1) p = i;
            else if (a[i][m+n] / a[i][q] < a[p][m+n] / a[p][q])
                p = i;
        }
        return p;
    }
    void pivot(int p, int q) {
        for (int i = 0; i <= m; i++)
            for (int j = 0; j <= m+n; j++)
                if (i != p && j != q)
                    a[i][j] -= a[p][j] * a[i][q] / a[p][q];
        for (int i = 0; i <= m; i++)
            if (i != p) a[i][q] = 0.0;
        for (int j = 0; j <= m+n; j++)
            if (j != q) a[p][j] /= a[p][q];
        a[p][q] = 1.0;
    }

private:
    std::vector<std::vector<double>> a; // simplex tableaux
    int m;
    int n; // M constraints, N variables
};


#endif //SIMPLEX_SIMPLEX_H
