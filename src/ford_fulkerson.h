//
// Created by Dmitry Sergeev on 10/21/16.
//

#ifndef SIMPLEX_FORD_FULKERSON_H
#define SIMPLEX_FORD_FULKERSON_H

#include "flow_network.h"

#include <algorithm>
#include <queue>

template<typename value_t>
class ford_fulkerson {
	static_assert(std::is_scalar<value_t>::value, "template argument must be a scalar type");
public:
    ford_fulkerson(const flow_network<value_t> &G, const int s, const int t): _value(value_t(0)) {
        marked.resize(G.size(), false);
        edge_to.resize(G.size(), nullptr);
        while (has_augmenting_path(G, s, t)) {
            value_t bottle = std::numeric_limits<value_t>::max();
            for (int v = t; v != s; v = edge_to[v]->other(v))
                bottle = std::min(bottle, edge_to[v]->residual_capacity_to(v));
            for (int v = t; v != s; v = edge_to[v]->other(v))
                edge_to[v]->add_residual_flow_to(v, bottle);
            _value += bottle;
        }
    }
    bool has_augmenting_path(const flow_network<value_t> &G, const int s, const int t) {
        std::fill_n(marked.begin(), G.size(), false);
        std::fill_n(edge_to.begin(), G.size(), nullptr);
        std::queue<int> queue;
        queue.push(s);
        marked[s] = true;
        while (!queue.empty()) {
            int v = queue.front();
            queue.pop();
            for (auto e : G.adj(v)) {
                auto w = e->other(v);
                if (e->residual_capacity_to(w) > 0 && !marked[w]) {
                    edge_to[w] = e;
                    marked[w] = true;
                    queue.push(w);
                }
            }
        }
        return marked[t];
    }
    value_t value() const  { return _value; }
    bool in_cut(int v) const { return marked[v]; }
private:
    std::vector<bool> marked; // true if s->_from path in residual network
    std::vector<std::shared_ptr<flow_edge<value_t>>> edge_to; // last edge on s->_from path
    value_t _value; // value of flow
};


#endif //SIMPLEX_FORD_FULKERSON_H
