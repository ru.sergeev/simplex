//
// Created by Dmitry Sergeev on 10/20/16.
//

#ifndef SIMPLEX_FLOWNETWORK_H
#define SIMPLEX_FLOWNETWORK_H

#include "flow_edge.h"

#include <vector>
#include <algorithm>
#include <memory>

template<typename value_t>
class flow_network {
	static_assert(std::is_scalar<value_t>::value, "template argument must be a scalar type");
public:
    flow_network(int V) : V(V), _adj(std::vector<std::vector<std::shared_ptr<flow_edge<value_t>>>>(V)) {
        std::fill_n(_adj.begin(), V, std::vector<std::shared_ptr<flow_edge<value_t>>>());
    }
    void add_edge(flow_edge<value_t>&& e) {
        auto edge = std::make_shared<flow_edge<value_t>>(e);
        int v = edge->from();
        int w = edge->to();
        _adj[v].push_back(edge);
        _adj[w].push_back(edge);
    }
     const std::vector<std::shared_ptr<flow_edge<value_t>>>& adj(int v) const
    { return _adj[v]; }
    int size() const {return V;}
private:
    const int V;
    std::vector<std::vector<std::shared_ptr<flow_edge<value_t>>>> _adj;
};

#endif //SIMPLEX_FLOWNETWORK_H
