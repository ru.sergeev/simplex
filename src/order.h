//
// Created by Dmitry Sergeev on 10/24/16.
//

#ifndef SIMPLEX_ORDER_H
#define SIMPLEX_ORDER_H

/// order
struct order{
    order(int id, int time, int cod, int haddock, int chips):
            id(id), time(time), cod(cod), haddock(haddock), chips(chips){}
    int id;
    int time;
    int cod;
    int haddock;
    int chips;
};



#endif //SIMPLEX_ORDER_H
